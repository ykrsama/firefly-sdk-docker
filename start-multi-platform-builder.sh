if [[ $(docker ps 2>&1 |cut -c1-17) == "permission denied" ]]; then
    sudo docker buildx create --use --platform=linux/arm64,linux/amd64 --name multi-platform-builder
    sudo docker buildx inspect --bootstrap
else 
    docker buildx create --use --platform=linux/arm64,linux/amd64 --name multi-platform-builder
    docker buildx inspect --bootstrap
fi

