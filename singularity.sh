basedir=$(dirname $(which $1))

sif_file="${basedir}/firefly-sdk_latest.sif"
#sbox_dir="/tmp/${USER}.firefly-sdk_latest.sbox"
sbox_dir="${basedir}/firefly-sdk_latest.sbox"

# Download docker file
if [[ ! -f $sif_file ]]; then
    singularity pull $sif_file docker://ykrsama/firefly-sdk:latest
    singularity build --sandbox $sbox_dir $sif_file
fi

if [[ ! -d $sbox_dir ]]; then
    # convert SIF to sandbox
    singularity build --sandbox $sbox_dir $sif_file
fi

# run
mkdir -p /tmp/.X11-unix # To fix container creation failed
echo "Loading sandbox: $sbox_dir"
singularity run --bind $PWD:$PWD,/tmp/.X11-unix:/tmp/.X11-unix:ro --pwd $PWD \
    --env DISPLAY=$DISPLAY --no-home --contain --cleanenv $sbox_dir "$@"
