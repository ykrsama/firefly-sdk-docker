#!/bin/bash
set -e 

cd /opt/root/bin && source thisroot.sh
cd /opt/geant4/bin && source geant4.sh

# ACTS
ACTS_DIR="/opt/acts"
export PATH="${ACTS_DIR}/bin:${PATH}"
export LD_LIBRARY_PATH="${ACTS_DIR}/lib:${LD_LIBRARY_PATH}"

exec "$@"
