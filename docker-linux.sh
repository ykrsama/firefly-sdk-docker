docker_tag=ykrsama/firefly-sdk:0.0.1.x86_64

mkdir -p run
sudo docker run -it --net=host --rm -e DISPLAY=$DISPLAY \
    -v /tmp/.X11-unix:/tmp/.X11-unix:ro \
    -v $PWD:$PWD -w $PWD \
    ${docker_tag} "$@"
