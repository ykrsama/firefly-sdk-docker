if [[ $(docker ps 2>&1 |cut -c1-17) == "permission denied" ]]; then
    sudo DOCKER_BUILDKIT=1 docker buildx build \
        --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
        --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
        --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
        --push \
        --platform linux/amd64,linux/arm64 \
        --tag ykrsama/firefly-sdk:$(cat tag) \
        --tag ykrsama/firefly-sdk:latest .
else
    DOCKER_BUILDKIT=1 docker buildx build \
        --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
        --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
        --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
        --push \
        --platform linux/amd64,linux/arm64 \
        --tag ykrsama/firefly-sdk:$(cat tag) \
        --tag ykrsama/firefly-sdk:latest .
fi


