# syntax=docker/dockerfile:1
FROM ubuntu:22.04 AS base
        LABEL maintainer.name="Xuliang Zhu"
        LABEL version="20240220"
        LABEL maintainer.email="xuliangz@sjtu.edu.cn"
        
        SHELL ["/bin/bash", "-c"]
        
        ENV LANG=C.UTF-8
        
        # install general dependencies
        COPY packages packages
        
        ARG DEBIAN_FRONTEND=noninteractive
        ARG UBUNTU_MIRROR=http://mirrors.ustc.edu.cn/
        
        RUN sed -i "s#http://archive.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            sed -i "s#http://ports.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            sed -i "s#http://security.ubuntu.com/#${UBUNTU_MIRROR}#g" /etc/apt/sources.list &&\
            apt-get update &&\
            ln -sf /usr/share/zoneinfo/UTC /etc/localtime &&\
            apt-get -y install curl wget git vim gdb valgrind linux-tools-generic $(cat packages) &&\
            dpkg-reconfigure locales &&\
            apt-get autoremove -y &&\
            apt-get clean &&\
            rm -rf /var/lib/apt/lists/* &&\
            rm /usr/bin/perf &&\
            ln -s /usr/lib/linux-tools/*-generic/perf /usr/bin/perf &&\
            strip --remove-section=.note.ABI-tag /usr/lib/*-linux-gnu/libQt5Core.so &&\
            ldconfig
         # replace perf to fix the error:"perf not found for kernel xxx"
         # strip is to solve the problem in qt5 which checks the compatibility with the host kernel.

# Build ROOT
FROM base AS root
        ARG ROOT_VERSION=6.30.04
        
        WORKDIR /root
                ADD Downloads/root_v${ROOT_VERSION}.source.tar.gz .
                
                RUN mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=/opt/root \
                          ../root-${ROOT_VERSION} &&\
                    cmake --build . --target install -- -j$(nproc)

# Build Geant4
FROM base AS geant4
        ARG G4_VERSION=11.2.1
        
        WORKDIR /root
                ADD Downloads/geant4-v${G4_VERSION}.tar.gz .
                
                RUN mkdir -p build &&\
                    cd build &&\
                    cmake -DCMAKE_INSTALL_PREFIX=/opt/geant4 \
                          -DGEANT4_INSTALL_DATA=OFF \
                          -DGEANT4_USE_GDML=ON \
                          -DGEANT4_USE_QT=ON \
                          -DGEANT4_USE_PYTHON=ON \
                          ../geant4-v${G4_VERSION} &&\
                    make -j$(nproc) install
                
                COPY Downloads/geant4-data-v${G4_VERSION} /opt/geant4/share/Geant4/data

# Build ACTS
FROM base AS acts
        # From branch xuliang-v30
        ARG ACTS_BRANCH="xuliang-v30"
        ARG ACTS_GIT_URL="https://github.com/ykrsama/acts.git"
        #ARG ACTS_GIT_URL="https://github.com/acts-project/acts.git"

        COPY --from=root /opt/root /opt/root
        COPY --from=geant4 /opt/geant4 /opt/geant4
        
        RUN cd /opt/root/bin &&\
            source thisroot.sh &&\
            cd /opt/geant4/bin &&\
            source geant4.sh &&\
            cd /root &&\
            git clone --branch ${ACTS_BRANCH} --depth=1 ${ACTS_GIT_URL} &&\
            cd acts &&\
            git checkout 88a3dd1080412e087974153404f72a76b41e27e9 &&\
            cd .. &&\
            mkdir -p build &&\
            cd build &&\
            cmake -Wno-dev \
                  -DCMAKE_CXX_STANDARD=17 \
                  -DCMAKE_BUILD_TYPE=Debug \
                  -DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
                  -DCMAKE_INSTALL_PREFIX=/opt/acts \
                  -DACTS_BUILD_FATRAS=OFF \
                  -DACTS_BUILD_PLUGIN_TGEO=ON \
                  -DACTS_BUILD_PLUGIN_JSON=ON \
                  -DACTS_BUILD_EXAMPLES=ON \
                  -DACTS_BUILD_EXAMPLES_PYTHON_BINDINGS=OFF \
                  -DACTS_BUILD_EXAMPLES_BINARIES=ON \
                  ../acts &&\
            make -j$(nproc) install
        RUN cp /root/build/compile_commands.json /root/acts

FROM base AS vim_ycm
        ENV MAKEFLAGS=-j$(nproc)

        COPY root /root

        RUN curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
            https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

        RUN vim +PlugInstall +qall

        WORKDIR /root/.vim/plugged/YouCompleteMe
                RUN ./install.py
        # Do some clean-up
        WORKDIR /root/.vim/plugged
                RUN rm -rf */.git

# Release
FROM base AS release
        WORKDIR /root
        COPY --from=root /opt/root /opt/root
        COPY --from=geant4 /opt/geant4 /opt/geant4
        COPY --from=acts /opt/acts /opt/acts
        COPY --from=acts /root/acts /root/acts
        COPY --from=vim_ycm /root/.vim /root/.vim
        # Copy scripts
        COPY root /root
        COPY entry-point.sh /entry-point.sh
        
        ENTRYPOINT ["/entry-point.sh"]
        
        CMD bash
