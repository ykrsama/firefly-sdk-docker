docker_tag="ykrsama/firefly-sdk:$(cat tag).$(uname -m)"

if [[ $(docker ps 2>&1 |cut -c1-17) == "permission denied" ]]; then
    sudo DOCKER_BUILDKIT=1 docker buildx build \
        --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
        --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
        --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
        --load \
        --platform linux/$(uname -m) \
        --tag ${docker_tag} \
        . &&\
    sudo docker push ${docker_tag}
else
    DOCKER_BUILDKIT=1 docker buildx build \
        --secret id=id_rsa,src=$HOME/.ssh/id_rsa \
        --secret id=id_rsa.pub,src=$HOME/.ssh/id_rsa.pub \
        --secret id=known_hosts,src=$HOME/.ssh/known_hosts \
        --load \
        --platform linux/$(uname -m) \
        --tag ${docker_tag} \
        . &&\
    docker push ${docker_tag}
fi


